import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import dash_table
import plotly.plotly as py
import plotly.graph_objs as go
import numpy as np

from PixDb import *

db=PixDb(os.environ["PIX_MOD_TRACKER_PATH"]+"/.ModuleTracker.json")


app = dash.Dash(__name__)

app.layout = html.Div([
    html.H1('Pixel Module Tracker Database'),
     dcc.Tabs(id="Tab", value='tab-module-list', children=[
        dcc.Tab(label='Module List', value='tab-module-list'),
        dcc.Tab(label='Hypothesis Correlation', value='Hypothesis'),
        dcc.Tab(label='Diagnosis Correlation', value='Diagnosis'),
    ]),
    html.Div(id='tab-content')
])

@app.callback(Output('tab-content', 'children'),
              [Input('Tab', 'value')])
def render_content(tab):
    if tab == 'tab-module-list':
        query=db.query_all_todict()
        data=list()
        cols=db.col_names
        cols.remove("ID")
        cols.remove("Entered")

        for q in query:
            d=dict()
            d=query[q]
            del d["ID"]
            del d["Entered"]
            for k in ("Symptoms","Diagnosis","Actions","Hypothesis"):
                d[k]=d[k].replace(",","\n")
            d["MID"]=q
            data.append(d)
        return dash_table.DataTable(
            id='table',
            style_data={'whiteSpace': 'pre'},
            css=[{
                    'selector': '.dash-cell div.dash-cell-value',
                    'rule': 'display: inline; white-space: inherit; overflow: inherit; text-overflow: inherit;'
                    }],
            columns=[{"name": i, "id": i, "deletable": True} for i in cols],
            data=data,
            sorting=True,
            sorting_type="multi",
            filtering=True
            )
    elif tab == 'Diagnosis' or tab == 'Hypothesis':
        query=db.query_all_todict(toStrings=False)
        map=list()
        nx=len(db.symptoms)
        ny=len(db.diagnosis)
        ylabels=db.diagnosis[0:15] + ["Disabled FE"] + db.diagnosis[-3:]
        for i in range(ny-15):
            xl=[0]*(nx)
            map.append(xl)
            
        for q in query:            
            xc=query[q]["Symptoms"]
            yc=query[q][tab]
            for x in range(nx):
                yr=0
                for y in range(ny):
                    if (xc&(1<<x)>0)  and (yc & (1<<y) > 0):
                        map[yr][x]=map[yr][x]+1
                    if(y<15 or y>29): yr=yr+1 
        return dcc.Graph(
            id='heatmap',
            figure={
                'data': [{
                        'x': db.symptoms,
                        'y': ylabels,
                        'z': map,
                        "colorscale": 'Hot',
                        "reversescale": True,
                        'type': 'heatmap'
                        }],
                "layout": {
                    "font": dict(size=18),
                    "title":tab,
                    "xaxis": {
                    "automargin" : True ,"showgrid":True, "showline":True },

                    "yaxis": {
                    "automargin" : True  ,"showgrid":True, "showline":True  },
                    "height": 1200  # px
                    }
                }
            )

if __name__ == '__main__':
    app.run_server(debug=False,host="0.0.0.0")
