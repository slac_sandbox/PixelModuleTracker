#!/usr/bin/env python

from PixDb import *
import os
import csv
import argparse


db=PixDb(os.environ["PIX_MOD_TRACKER_PATH"]+"/.ModuleTracker.json")
username="superuser"
observed="2018-06-06 12:00:00"
symptoms=db.add_symptom('No hits on track')
symptoms=db.add_symptom('Low occupancy',symptoms)
actions=db.add_action('Needs Work')
diagnosis=db.add_diagnosis('HV open')
diagnosis=db.add_diagnosis('NTC open',diagnosis)
hypothesis=db.add_diagnosis('HV open')
hypothesis=db.add_diagnosis('NTC open',hypothesis)
priority=db.get_priority("Urgent")


record= {
  "timestamp": observed,
  "actions":actions,
  "symptoms": symptoms,
  "username": username,
  "priority": priority,
  "diagnosis": diagnosis,
  "hypothesis" : hypothesis,
  "comment" : "Some comment here"
}
mid="M512909"
valid=db.check_record(mid,record)
if valid:
    db.insert(mid,record)
    print(db.query_all())
