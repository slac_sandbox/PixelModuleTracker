Python GUI to track problematic pixel modules in the Atlas conditions DB

Currently, due to package dependencies on Qt5 and PyQT5, this only runs on lxplus

Setup

git clone https://:@gitlab.cern.ch:8443/slac_sandbox/PixelModuleTracker.git

cd PixelModuleTracker

cp .ModuleTracker.json ~

Edit to configuration file ~/.ModuleTracker.json and update the password
 
  "PASSWORD": "Change me to a valid password"
 
 source setup.sh

 python ModuleTracker.py