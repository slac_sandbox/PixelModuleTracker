
drop table history ;
drop sequence history_seq;

create table history (
 id            number(10) check (id > 0)     NOT NULL,  
 moduleserial  varchar2(16),
 observed      timestamp(0) DEFAULT SYSTIMESTAMP  NOT NULL  ,
 systemdate    timestamp(0) DEFAULT SYSTIMESTAMP  NOT NULL  ,
 username      varchar2(80),    
 "COMMENT"     varchar2(1024),  
 actions       varchar2(256),   
 priority      number(10),   
 diagnosis     varchar2(256),
 hypothesis    varchar2(256),   
 symptoms      varchar2(256)
);

create sequence history_seq start with 1 increment by 1;

create or replace trigger history_seq_tr
 before insert on history for each row
 when (new.id is null)
begin
 select history_seq.nextval into :new.id from dual;
end;
/

drop table history_diagnosis;
drop sequence history_diagnosis_seq;
create table history_diagnosis (
  id           number(10) not null primary key,
  label        varchar2(80),
  description  varchar2(1024)
);
create sequence history_diagnosis_seq start with 1 increment by 1;
create or replace trigger history_diagnosis_seq_tr
 before insert on history_diagnosis
 for each row
begin
 select history_diagnosis_seq.nextval into :new.id from dual;
end;
/

drop table history_priorities;
drop sequence history_priorities_seq;
create table history_priorities (
  id           number(10) not null primary key,
  label        varchar2(80)
);
create sequence history_priorities_seq start with 1 increment by 1;
create or replace trigger history_priorities_seq_tr
 before insert on history_priorities
 for each row
begin
 select history_priorities_seq.nextval into :new.id from dual;
end;
/

drop table  history_symptoms;
drop sequence  history_symptoms_seq;

create table history_symptoms (
 id           number(10) NOT NULL primary key ,
 label        varchar2(80),      
 description  varchar2(1024)     
);

create sequence history_symptoms_seq start with 1 increment by 1;

create or replace trigger history_symptoms_seq_tr
 before insert on history_symptoms for each row
 when (new.id is null)
begin
 select history_symptoms_seq.nextval into :new.id from dual;
end;
/

drop table  history_actions;
drop sequence  history_actions_seq;

create table history_actions (
  id           number(10) not null primary key,
  label        varchar2(80),
  description  varchar2(1024)
);

create sequence history_actions_seq start with 1 increment by 1;

create or replace trigger history_actions_seq_tr
 before insert on history_actions for each row
 when (new.id is null)
begin
 select history_actions_seq.nextval into :new.id from dual;
end;
/

insert into  history_priorities (LABEL) VALUES('YETS');
insert into  history_priorities (LABEL) VALUES('Low');
insert into  history_priorities (LABEL) VALUES('Medium');
insert into  history_priorities (LABEL) VALUES('High');
insert into  history_priorities (LABEL) VALUES('Urgent');

insert into  history_actions (LABEL) VALUES('Needs Monitoring');
insert into  history_actions (LABEL) VALUES('Needs Work');
insert into  history_actions (LABEL) VALUES('Disable in DAQ');
insert into  history_actions (LABEL) VALUES('Disable NTC');
insert into  history_actions (LABEL) VALUES('Disable in DCS');
insert into  history_actions (LABEL) VALUES('Unfixable in DCS');

insert into  history_diagnosis (LABEL) VALUES('Unknown');
insert into  history_diagnosis (LABEL) VALUES('HV open');
insert into  history_diagnosis (LABEL) VALUES('NTC open');
insert into  history_diagnosis (LABEL) VALUES('LV open');
insert into  history_diagnosis (LABEL) VALUES('DCI open');
insert into  history_diagnosis (LABEL) VALUES('DTO open');
insert into  history_diagnosis (LABEL) VALUES('Clock open');
insert into  history_diagnosis (LABEL) VALUES('Sense open');
insert into  history_diagnosis (LABEL) VALUES('PP2 bad');
insert into  history_diagnosis (LABEL) VALUES('PP4 bad');
insert into  history_diagnosis (LABEL) VALUES('TX bad');
insert into  history_diagnosis (LABEL) VALUES('RX bad');
insert into  history_diagnosis (LABEL) VALUES('VCSEL bad');
insert into  history_diagnosis (LABEL) VALUES('DORIC bad');
insert into  history_diagnosis (LABEL) VALUES('Losing clock');
insert into  history_diagnosis (LABEL) VALUES('Has Disabled Frontend 0');
insert into  history_diagnosis (LABEL) VALUES('Has Disabled Frontend 1');
insert into  history_diagnosis (LABEL) VALUES('Has Disabled Frontend 2');
insert into  history_diagnosis (LABEL) VALUES('Has Disabled Frontend 3');
insert into  history_diagnosis (LABEL) VALUES('Has Disabled Frontend 4');
insert into  history_diagnosis (LABEL) VALUES('Has Disabled Frontend 5');
insert into  history_diagnosis (LABEL) VALUES('Has Disabled Frontend 6');
insert into  history_diagnosis (LABEL) VALUES('Has Disabled Frontend 7');
insert into  history_diagnosis (LABEL) VALUES('Has Disabled Frontend 8');
insert into  history_diagnosis (LABEL) VALUES('Has Disabled Frontend 9');
insert into  history_diagnosis (LABEL) VALUES('Has Disabled Frontend 10');
insert into  history_diagnosis (LABEL) VALUES('Has Disabled Frontend 11');
insert into  history_diagnosis (LABEL) VALUES('Has Disabled Frontend 12');
insert into  history_diagnosis (LABEL) VALUES('Has Disabled Frontend 13');
insert into  history_diagnosis (LABEL) VALUES('Has Disabled Frontend 14');
insert into  history_diagnosis (LABEL) VALUES('Has Disabled Frontend 15');
insert into  history_diagnosis (LABEL) VALUES('MCC broken');
insert into  history_diagnosis (LABEL) VALUES('Sensor bad);

insert into  history_symptoms (LABEL) VALUES('No hits on track');
insert into  history_symptoms (LABEL) VALUES('Low occupancy');
insert into  history_symptoms (LABEL) VALUES('High noise');
insert into  history_symptoms (LABEL) VALUES('Low HV current');
insert into  history_symptoms (LABEL) VALUES('NTC flaky');
insert into  history_symptoms (LABEL) VALUES('NTC disconnected');
insert into  history_symptoms (LABEL) VALUES('No VDDA current');
insert into  history_symptoms (LABEL) VALUES('No VDD current');
insert into  history_symptoms (LABEL) VALUES('No IBL LV current');
insert into  history_symptoms (LABEL) VALUES('High VDDA current');
insert into  history_symptoms (LABEL) VALUES('High VDD current');
insert into  history_symptoms (LABEL) VALUES('High IBL LV current');
insert into  history_symptoms (LABEL) VALUES('Low VDDA current');
insert into  history_symptoms (LABEL) VALUES('Low VDD current');
insert into  history_symptoms (LABEL) VALUES('Low IBL LV current');
insert into  history_symptoms (LABEL) VALUES('Unstable VDDA current');
insert into  history_symptoms (LABEL) VALUES('Unstable VDD current');
insert into  history_symptoms (LABEL) VALUES('Unstable IBL LV current');
insert into  history_symptoms (LABEL) VALUES('VDD does not change with VVDC');
insert into  history_symptoms (LABEL) VALUES('Low PIN current');
insert into  history_symptoms (LABEL) VALUES('Bad inlink scan');
insert into  history_symptoms (LABEL) VALUES('BadNo EFR');
insert into  history_symptoms (LABEL) VALUES('Bad loopback test');
insert into  history_symptoms (LABEL) VALUES('Dark (power meter)');
insert into  history_symptoms (LABEL) VALUES('Does not configure');
insert into  history_symptoms (LABEL) VALUES('Startup failed');
insert into  history_symptoms (LABEL) VALUES('Desynchronized');
insert into  history_symptoms (LABEL) VALUES('Bad digital test');
insert into  history_symptoms (LABEL) VALUES('Bad analog test');
insert into  history_symptoms (LABEL) VALUES('Timeouts');
insert into  history_symptoms (LABEL) VALUES('Data corruption');


